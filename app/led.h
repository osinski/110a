/*
 * led.h
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef LED_H
#define LED_H

#include <array>
#include <stdint.h>

#include "../sdk/drivers/fsl_gpio.h"

class LED
{
  public:
    LED(const uint32_t ledPort, const uint32_t ledPin)
      : port{ ledPort }
      , pin{ ledPin }
    {}

    LED() = delete;
    LED(const LED&) = delete;
    LED(LED&&) = delete;

    void setup() const
    {
        const gpio_pin_config_t ledConf = { kGPIO_DigitalOutput, 0 };

        GPIO_PinInit(GPIO, port, pin, &ledConf);
    }

    void turnOn() const { GPIO_PinWrite(GPIO, port, pin, 1); }

    void turnOff() const { GPIO_PinWrite(GPIO, port, pin, 0); }

    void set(bool state) const
    {
        if (state) {
            turnOn();
        } else {
            turnOff();
        }
    }

  private:
    const uint32_t port;
    const uint32_t pin;
};

class CompartmentLED : public LED
{
  public:
    CompartmentLED(const uint32_t ledPort,
                   const uint32_t ledPin,
                   const uint8_t timeOff1,
                   const uint8_t timeOn1,
                   const uint8_t timeOff2,
                   const uint8_t timeOn2)
      : LED(ledPort, ledPin)
      , workingTimes{ timeOff1, timeOn1, timeOff2, timeOn2 }
    {}

    void newTick()
    {
        if (ticker <= workingTimes.s.tOff1) {
            turnOff();
        } else if (ticker > workingTimes.s.tOff1 && ticker < timesPeriod1) {
            turnOn();
        } else if (ticker >= timesPeriod1 &&
                   ticker < (timesPeriod1 + workingTimes.s.tOff2)) {
            turnOff();
        } else if (ticker >= (timesPeriod1 + workingTimes.s.tOff2) &&
                   ticker < timesSum) {
            turnOn();
        } else {
            ticker = 0;
            turnOff();
        }

        ticker++;
    }

    uint8_t* getWorkingTimesData()
    {
        return workingTimes.a;
    }

    void setWorkingTimesData(uint8_t* newTimings)
    {
        memcpy(workingTimes.a, newTimings, 4);
    }

    void setWorkingTimesData(std::array<uint8_t, 4> newTimings)
    {
        memcpy(workingTimes.a, newTimings.data(), 4);
    }

  private:
    uint32_t ticker = 0;

    union {
        struct {
            uint8_t tOff1;
            uint8_t tOn1;
            uint8_t tOff2;
            uint8_t tOn2;
        } s;
        uint8_t a[4];
    } workingTimes;

    const uint32_t timesSum = workingTimes.s.tOff1 + workingTimes.s.tOff2 +
                              workingTimes.s.tOn1 + workingTimes.s.tOn2;
    const uint32_t timesPeriod1 = workingTimes.s.tOff1 + workingTimes.s.tOn1;
    const uint32_t timesPeriod2 = workingTimes.s.tOff2 + workingTimes.s.tOn2;
};

#endif /* LED_H */
