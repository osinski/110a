/*
 * iap.cc
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "iap.h"
#include <string.h>

constexpr uint8_t flashSector = 15;
constexpr uint8_t flashPage = 240;
constexpr uint32_t addr = FSL_FEATURE_SYSCON_FLASH_PAGE_SIZE_BYTES * flashPage;

volatile IAPControls IAP::controlFlags = {false, false};


void IAP::init()
{
    IAP_ConfigAccessFlashTime(kFlash_IAP_OneSystemClockTime); 
}


void IAP::write(uint8_t* settings, std::size_t numOfBytes)
{

    IAP_PrepareSectorForWrite(flashSector, flashSector);
    IAP_ErasePage(flashPage, flashPage, SystemCoreClock);
    IAP_PrepareSectorForWrite(flashSector, flashSector);
    status_t res = 
        IAP_CopyRamToFlash(addr, reinterpret_cast<uint32_t*>(settings),
                           numOfBytes, SystemCoreClock);

    IAP::controlFlags.triggerWrite = false;
}

void IAP::read(uint8_t* settings, std::size_t numOfBytes)
{
    uint8_t* flashSector = reinterpret_cast<uint8_t*>(addr);
    memcpy(settings, flashSector, numOfBytes);
    //nonVolatileSettings.s = { static_cast<bool>(flashSector[0]),
    //                          static_cast<bool>(flashSector[1]),
    //                          static_cast<bool>(flashSector[2]),
    //                          static_cast<bool>(flashSector[3]) };
}

void IAP::countdown()
{
    constexpr uint8_t countDownValue = 30;
    static uint8_t counter = countDownValue;

    if (IAP::controlFlags.countdownEnabled && (--counter == 0)) {
        IAP::controlFlags.triggerWrite = true;
        IAP::controlFlags.countdownEnabled = false;

        counter = countDownValue;
    }
}

void IAP::enableCountdown()
{
    controlFlags.countdownEnabled = true;
}

bool IAP::writeTriggered()
{
    return controlFlags.triggerWrite;
}

