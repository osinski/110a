/*
 * reedswitch.h
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef REEDSWITCH_H
#define REEDSWITCH_H

#include "../sdk/drivers/fsl_pint.h"
#include "../sdk/drivers/fsl_syscon.h"
#include "../sdk/drivers/fsl_syscon_connections.h"

struct ReedSwitch
{
    ReedSwitch(uint32_t swPort, uint32_t swPin, pint_pin_int_t pintNumber)
      : port{ swPort }
      , pin{ swPin }
      , pintIntr{ pintNumber }
    {}

    const uint32_t port;
    const uint32_t pin;
    const pint_pin_int_t pintIntr;

    bool state = false;

    void setup() const
    {
        const syscon_connection_t sysconn =
          static_cast<syscon_connection_t>(pin + (PINTSEL_ID << SYSCON_SHIFT));
        SYSCON_AttachSignal(SYSCON, pintIntr, sysconn);

        PINT_PinInterruptConfig(
          PINT, pintIntr, kPINT_PinIntEnableFallEdge, NULL);
        NVIC_EnableIRQ(static_cast<IRQn_Type>(24 + pintIntr));

        PINT_EnableCallbackByIndex(PINT, pintIntr);
    }

    void debounce() { PINT_PinInterruptClrStatus(PINT, pintIntr); }
};

#endif /* REEDSWITCH_H */
