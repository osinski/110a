/*
 * iap.h
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#ifndef IAP_H
#define IAP_H

#include "../sdk/LPC822.h"
#include "../sdk/drivers/fsl_iap.h"

struct IAPControls
{
    bool countdownEnabled;
    bool triggerWrite;
};


namespace IAP{
    void init();
    void write(uint8_t* settings, std::size_t numOfBytes);
    void read(uint8_t* settings, std::size_t numOfBytes);
    void countdown();

    void enableCountdown();

    bool writeTriggered();

    extern volatile IAPControls controlFlags;
}

#endif /* IAP_H */
