/*
 * main.cc
 * Copyright (C) 2021 Marcin Osiński <osinski.marcin.r@gmail.com>
 *
 * Distributed under terms of the MIT license.
 */

#include "../sdk/LPC822.h"

#include "../sdk/drivers/fsl_clock.h"
#include "../sdk/drivers/fsl_power.h"
#include "../sdk/drivers/fsl_usart.h"
#include "../sdk/drivers/fsl_swm.h"
#include "../sdk/drivers/fsl_dma.h"
#include "../sdk/drivers/fsl_adc.h"

#include <array>

#include "led.h"
#include "iap.h"
#include "reedswitch.h"
#include "regops.h"

static std::array<CompartmentLED, 10> compartmentLEDs{
    CompartmentLED(0, 13, 20, 10, 100, 50),
    CompartmentLED(0, 28, 40, 70, 230, 100),
    CompartmentLED(0, 11, 30, 50, 200, 120),
    CompartmentLED(0, 27, 70, 80, 180, 150),
    CompartmentLED(0, 26, 90, 90, 170, 230),
    CompartmentLED(0, 25, 10, 200, 150, 230),
    CompartmentLED(0, 24, 50, 100, 140, 170),
    CompartmentLED(0, 15, 50, 120, 90, 200),
    CompartmentLED(0, 1, 100, 60, 210, 250),
    CompartmentLED(0, 9, 105, 40, 220, 170)
};

static std::array<LED, 3> corridorLEDs{ LED{ 0, 18 },
                                        LED{ 0, 14 },
                                        LED{ 0, 6 } };

static LED endLight1LED{ 0, 10 };
static LED endLight2LED{ 0, 16 };

static ReedSwitch endLight1Switch{ 0, 17, kPINT_PinInt0 };
static ReedSwitch endLight2Switch{ 0, 7, kPINT_PinInt1 };
static ReedSwitch mainSwitch{ 0, 8, kPINT_PinInt2 };

union
{
    struct {
        bool randomize;
        bool endLight1Enabled;
        bool endLight2Enabled;
        bool mainLightEnabled;
        uint8_t compartmentsTimings[40];
        uint8_t reserved[20];
    } s;
    uint8_t a[64];
} nonVolatileSettings;


volatile static struct
{
    bool tick = false;
    bool enable = false;
} mainLightBlinkingFlags;

volatile static struct
{
    bool hello = false;
    bool timingsRecvd = false;
} dmaRxFlags;

static dma_descriptor_t  __attribute__((aligned(16))) dmaDescriptor;
dma_handle_t dmaUartRxHandle;
adc_result_info_t adcResult;

static uint8_t uartDmaBuffer[44];

static void HWInit();

extern "C" int
main()
{
    HWInit();

    IAP::init();

    mainSwitch.setup();
    endLight1Switch.setup();
    endLight2Switch.setup();

    for (auto& led : compartmentLEDs) {
        led.setup();
        led.turnOff();
    }

    for (auto& led : corridorLEDs) {
        led.setup();
        led.turnOff();
    }

    endLight1LED.setup();
    endLight2LED.setup();
    endLight1LED.turnOff();
    endLight2LED.turnOff();

    if (GPIO_PinRead(GPIO, 0, 19) == 0) {
        // led test
        endLight1LED.turnOn();
        endLight2LED.turnOn();

        for (auto& led : corridorLEDs) {
            led.turnOn();
        }

        for (auto& led : compartmentLEDs) {
            led.turnOn();

            SDK_DelayAtLeastUs(100'000, CLOCK_GetFreq(kCLOCK_CoreSysClk));
        }
    }

    IAP::read(nonVolatileSettings.a, sizeof(nonVolatileSettings.a));

    if (nonVolatileSettings.s.randomize) {
        ADC_DoSoftwareTriggerConvSeqA(ADC0);

        while (!ADC_GetChannelConversionResult(ADC0, 3, &adcResult)) {}
        ADC_GetConvSeqAGlobalConversionResult(ADC0, &adcResult);

        srand(adcResult.result);

        for (auto& led : compartmentLEDs) {
            led.setWorkingTimesData({ static_cast<uint8_t>(rand() & 255),
                                      static_cast<uint8_t>(rand() & 255),
                                      static_cast<uint8_t>(rand() & 255),
                                      static_cast<uint8_t>(rand() & 255) });
        }
    } else {
        for (uint8_t i = 0; i < compartmentLEDs.size(); i++) {
            compartmentLEDs[i].setWorkingTimesData(
                                &nonVolatileSettings.s.compartmentsTimings[4*i]);
        }
    }

    if (nonVolatileSettings.s.mainLightEnabled) {
        for (auto& led : corridorLEDs) {
            led.turnOn();
        }

        mainLightBlinkingFlags.enable = true;
    }

    endLight1LED.set(nonVolatileSettings.s.endLight1Enabled);
    endLight2LED.set(nonVolatileSettings.s.endLight2Enabled);

    while (true) {
        if (mainLightBlinkingFlags.tick) {
            for (auto& led : compartmentLEDs) {
                led.newTick();
            }

            mainLightBlinkingFlags.tick = false;
        }

        if (IAP::writeTriggered()) {
            IAP::write(nonVolatileSettings.a, sizeof(nonVolatileSettings.a));
        }

        if (dmaRxFlags.hello) {
            dmaRxFlags.hello = false;
            USART_WriteBlocking(USART0, nonVolatileSettings.a, 
                                sizeof(uartDmaBuffer));
        }

        if (dmaRxFlags.timingsRecvd) {
            dmaRxFlags.timingsRecvd = false;
            memcpy(nonVolatileSettings.a, uartDmaBuffer, 
                    sizeof(uartDmaBuffer));
            for (uint8_t i = 0; i < compartmentLEDs.size(); i++) {
                compartmentLEDs[i].setWorkingTimesData(&uartDmaBuffer[4*i+4]);
            }
            endLight1LED.set(nonVolatileSettings.s.endLight1Enabled);
            endLight2LED.set(nonVolatileSettings.s.endLight2Enabled);
            if (nonVolatileSettings.s.mainLightEnabled) {
               for (auto& led : corridorLEDs) {
                   led.turnOn();
               }

               mainLightBlinkingFlags.enable = true;
            } else {
               for (auto& led : corridorLEDs) {
                   led.turnOff();
               }
               for (auto& led : compartmentLEDs) {
                   led.turnOff();
               }

               mainLightBlinkingFlags.enable = false;
            }
            IAP::enableCountdown();
        }
    }
}

void HWInit()
{
    usart_config_t cfg;
    constexpr auto dmaChannel = 0;
    dma_transfer_config_t dmaConfig;
    adc_config_t adcCfg;
    adc_conv_seq_config_t adcConfSeqCfg;

    CLOCK_SetCoreSysClkDiv(12); // 1MHz clk from 12MHz IRC Osc
    SystemCoreClockUpdate();

    // SysTick
    SysTick_Config(SystemCoreClock);

    CLOCK_EnableClock(kCLOCK_Uart0);
    CLOCK_SetClkDivider(kCLOCK_DivUsartClk, 1);
    CLOCK_EnableClock(kCLOCK_Adc);

    POWER_DisablePD(kPDRUNCFG_PD_ADC0);

    SWM_SetMovablePinSelect(SWM0, kSWM_USART0_TXD, kSWM_PortPin_P0_4);
    SWM_SetMovablePinSelect(SWM0, kSWM_USART0_RXD, kSWM_PortPin_P0_0);
    SWM_SetFixedPinSelect(SWM0, kSWM_ADC_CHN2, true);

    PINT_Init(PINT);

    USART_GetDefaultConfig(&cfg);
    cfg.baudRate_Bps = 9600;
    cfg.enableRx = true;
    cfg.enableTx = true;
    USART_Init(USART0, &cfg, CLOCK_GetFreq(kCLOCK_MainClk));

    DMA_Init(DMA0);

    DMA_EnableChannel(DMA0, dmaChannel);
    DMA_CreateHandle(&dmaUartRxHandle, DMA0, dmaChannel);
    DMA_PrepareTransfer(&dmaConfig, ((void*)((uint32_t)&USART0->RXDAT)), 
            uartDmaBuffer, 1, sizeof(uartDmaBuffer), kDMA_PeripheralToMemory,
            &dmaDescriptor);
    DMA_SubmitTransfer(&dmaUartRxHandle, &dmaConfig);
    dmaConfig.xfercfg.intA = true;
    dmaConfig.xfercfg.intB = false;
    DMA_CreateDescriptor(&dmaDescriptor, &dmaConfig.xfercfg, 
                         (void*)&USART0->RXDAT, uartDmaBuffer, &dmaDescriptor);

    adcCfg.enableLowPowerMode = false;
    adcCfg.clockDividerNumber = 1;
    adcCfg.voltageRange = kADC_HighVoltageRange;
    ADC_Init(ADC0, &adcCfg);
    
    adcConfSeqCfg.channelMask = (1 << 3); //should be ADC_3 on PIO0_23
    adcConfSeqCfg.triggerMask = 0;
    adcConfSeqCfg.triggerPolarity = kADC_TriggerPolarityPositiveEdge;
    adcConfSeqCfg.enableSingleStep = false;
    adcConfSeqCfg.enableSyncBypass = false;
    adcConfSeqCfg.interruptMode = kADC_InterruptForEachSequence;
    ADC_SetConvSeqAConfig(ADC0, &adcConfSeqCfg);
    ADC_EnableConvSeqA(ADC0, true);

    DMA_StartTransfer(&dmaUartRxHandle);
}


// Interrupts
extern "C"
{
    void SysTick_Handler(void)
    {
        if (mainLightBlinkingFlags.enable && !mainLightBlinkingFlags.tick) {
            mainLightBlinkingFlags.tick = true;
        }

        IAP::countdown();
    }

    void DMA0_IRQHandler(void)
    {
        if (uartDmaBuffer[0] == 0xFF) {
            dmaRxFlags.hello = true;
        } else {
            dmaRxFlags.timingsRecvd = true;
        }
        DMA_COMMON_REG_SET(DMA0, 0, INTA, 1);
    }

    void PIN_INT0_IRQHandler(void)
    {
        endLight1Switch.debounce();

        if (endLight1Switch.state) {
            endLight1LED.turnOff();
            endLight1Switch.state = false;
        } else {
            endLight1LED.turnOn();
            endLight1Switch.state = true;
        }

        nonVolatileSettings.s.endLight1Enabled = endLight1Switch.state;
        IAP::enableCountdown();
    }

    void PIN_INT1_IRQHandler(void)
    {
        endLight2Switch.debounce();

        if (endLight2Switch.state) {
            endLight2LED.turnOff();
            endLight2Switch.state = false;
        } else {
            endLight2LED.turnOn();
            endLight2Switch.state = true;
        }

        nonVolatileSettings.s.endLight2Enabled = endLight2Switch.state;
        IAP::enableCountdown();
    }

    void PIN_INT2_IRQHandler(void)
    {
        mainSwitch.debounce();

        if (mainSwitch.state) {
            mainSwitch.state = false;
            mainLightBlinkingFlags.enable = mainSwitch.state;

            for (auto& led : corridorLEDs) {
                led.turnOff();
            }
            for (auto& led : compartmentLEDs) {
                led.turnOff();
            }
        } else {
            mainSwitch.state = true;
            mainLightBlinkingFlags.enable = mainSwitch.state;

            for (auto& led : corridorLEDs) {
                led.turnOn();
            }
        }

        nonVolatileSettings.s.mainLightEnabled = mainSwitch.state;
        IAP::enableCountdown();
    }
}
