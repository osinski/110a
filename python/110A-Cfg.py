from PyQt5 import QtWidgets, uic
from PyQt5.QtCore import pyqtSlot
import serial
from serial.tools import list_ports
import struct
import sys


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)

        uic.loadUi('window.ui', self)

        self.tableWidget.horizontalHeader().setSectionResizeMode(
                                                QtWidgets.QHeaderView.Stretch
        )

        try:
            devices = list_ports.comports()
            ports = [d.device for d in devices] 
        except:
            ports = ["Brak urządzeń"]

        self.comboBox_availablePorts.addItems(ports)
        self.serialConn = serial.Serial()

    @pyqtSlot(QtWidgets.QTableWidgetItem, name='on_tableWidget_itemChanged')
    def itemChanged(self, item):
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Critical)
        msg.setText("BŁĄD")
        msg.setInformativeText("Podaj liczbę <= 255")
        msg.setWindowTitle("BŁĄD")

        try:
            val = int(item.text())
            if val > 255:
                val = 255
                msg.exec_()
            item.setText(str(val))
        except:
            item.setText("0")
            msg.exec_()

    @pyqtSlot(name='on_pushButton_connect_clicked')
    def connect(self):
        #print("connect clicked")
        portToConnect = self.comboBox_availablePorts.currentText()
        #print(portToConnect)
        if ((portToConnect != "Brak urządzeń") and \
            (portToConnect != "Wskaż konwerter USB <-> UART")):
            self.serialConn.setPort(self.comboBox_availablePorts.currentText())
            self.serialConn.open()
            #print(self.serialConn.isOpen())
            #print(self.serialConn.port)
            if self.checkBox_getTimings.isChecked():
                datalen = self.tableWidget.columnCount() * \
                            self.tableWidget.rowCount() + 4
                hellomsg = bytearray([255] * 44)
                #print(hellomsg)
                self.serialConn.write(hellomsg)
                timings = self.serialConn.read(datalen)
                timings = list(struct.unpack('<44B', timings))
                #print(timings)

                x = self.tableWidget.columnCount()
                y = self.tableWidget.rowCount()

                if timings[0] == 1:
                    self.radioButton_random.setChecked(True)
                elif timings[0] == 0:
                    self.radioButton_table.setChecked(True)

                if timings[1] == 1:
                    self.checkBox_endLight1.setChecked(True)
                elif timings[1] == 0:
                    self.checkBox_endLight1.setChecked(False)

                if timings[2] == 1:
                    self.checkBox_endLight2.setChecked(True)
                elif timings[2] == 0:
                    self.checkBox_endLight2.setChecked(False)

                if timings[3] == 1:
                    self.checkBox_mainLight.setChecked(True)
                elif timings[3] == 0:
                    self.checkBox_mainLight.setChecked(False)

                index = 4
                for i in range(y):
                    for j in range(x):
                        self.tableWidget.item(i,j).setText(str(timings[index]))
                        index += 1

            self.pushButton_connect.setEnabled(False)
            

    @pyqtSlot(name='on_pushButton_upload_clicked')
    def upload(self):
        #print("apply clicked")
        x = self.tableWidget.columnCount()
        y = self.tableWidget.rowCount()
        #print(x, 'cols')
        #print(y, 'rows')
        vals = [int(self.radioButton_random.isChecked()),
                int(self.checkBox_endLight1.isChecked()),
                int(self.checkBox_endLight2.isChecked()),
                int(self.checkBox_mainLight.isChecked())]

        for i in range(y):
            for j in range(x):
                #print('row {}, col {}'.format(i, j))
                #print(self.tableWidget.item(i, j).text())
                vals.append(int(self.tableWidget.item(i,j).text()))

        if self.radioButton_random.isChecked():
            vals[0] = 1

        #print(vals)
        #print(len(vals))

        packedVals = b''
        for val in vals:
            packed = struct.pack('<1B', val)
            #print(val)
            #print(packed)
            packedVals += packed

        #test = list(struct.unpack('<41B', packedVals))
        #print(test)

        self.serialConn.write(packedVals)


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    main = MainWindow()
    main.show()
    sys.exit(app.exec_())

